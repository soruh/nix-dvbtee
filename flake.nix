{
  description = "dvbtee";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.05;
  inputs.dvbtee.url = github:mkrufky/libdvbtee/v0.6.8;
  inputs.dvbtee.flake = false;
  inputs.libdvbpsi.url = "github:mkrufky/libdvbpsi/8cd5c202f9a3d5347a89bfa210cbf1a0c7e23c33";
  inputs.libdvbpsi.flake = false;

  # https://github.com/mkrufky/libdvbtee/archive/refs/tags/v0.6.8.tar.gz

  outputs = { self, nixpkgs, dvbtee, libdvbpsi }: {

    defaultPackage.x86_64-linux = let pkgs = import nixpkgs { system = "x86_64-linux"; }; in
      pkgs.stdenv.mkDerivation {
        name = "dvbtee";
        src = self;
        buildPhase = ''
          cp -r ${dvbtee} dvbtee
          chmod -R ug+w dvbtee
          pushd dvbtee
          cp -r ${libdvbpsi} libdvbpsi
          chmod -R ug+w libdvbpsi
          pushd libdvbpsi
          git apply ${self}/dvbteepsi.patch
          popd
          ./build-auto.sh
          popd
        '';
        installPhase = ''
          mkdir -p $out/bin $out/lib
          pushd dvbtee
          libs=(libdvbtee/value/.libs/libvalueobj.so.1 libdvbtee/decode/.libs/libdvbtee_decode.so.0 libdvbtee_server/.libs/libdvbtee_server.so.0 libdvbtee/.libs/libdvbtee.so.0 libdvbpsi/src/.libs/libdvbpsi.so.11)
          for lib in "''${libs[@]}"; do install -t $out/lib "$lib"; done
          popd
          install -t $out/bin dvbtee/dvbtee/.libs/dvbtee
          rpath="$(patchelf --print-rpath $out/bin/dvbtee | sed 's,/build/source/dvbtee/usr/lib,$out/lib/,')"
          echo "$rpath"
          patchelf --set-rpath "$rpath" $out/bin/dvbtee
          patchelf --set-rpath "$rpath" $out/lib/*
          patchelf --print-rpath $out/bin/dvbtee
        '';
        nativeBuildInputs = [ pkgs.git pkgs.automake pkgs.autoconf pkgs.libtool pkgs.file pkgs.pkg-config pkgs.patchelf ];
      };

  };
}